//
//  SearchResponseID.swift
//  Foursquare-client-ios
//
//  Created by Remi Robert on 12/01/2018.
//  Copyright © 2018 Remi Robert. All rights reserved.
//
import RealmSwift

public class SearchResponseDetail: Object, Codable {
    @objc dynamic var venue: Venue? = nil
}
