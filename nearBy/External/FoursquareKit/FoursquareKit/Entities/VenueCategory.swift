//
//  VenueCategory.swift
//  FoursquareAPIClient
//
//  Created by ogawa_kousuke on 2017/07/27.
//  Copyright © 2017年 Kosuke Ogawa. All rights reserved.
//
import RealmSwift

 class  VenueCategory: Object , Codable {
     @objc dynamic var categoryId: String = ""
     @objc dynamic var name: String = ""
     @objc dynamic var icon: VenueCategoryIcon? = nil

    private enum CodingKeys: String, CodingKey {
        case categoryId = "id"
        case name
        case icon
    }


}
