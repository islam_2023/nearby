//
//  VenueCategoryIcon.swift
//  FoursquareAPIClient
//
//  Created by ogawa_kousuke on 2017/07/27.
//  Copyright © 2017年 Kosuke Ogawa. All rights reserved.
//
import RealmSwift

 class VenueCategoryIcon:  Object , Codable {
    
     @objc dynamic var prefix: String = ""
     @objc dynamic var suffix: String = ""

    public var categoryIconUrl: String {
        return String(format: "%@%d%@", prefix, 88, suffix)
    }


}
