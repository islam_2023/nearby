//
//  LocalCache.swift
//  nearBy
//
//  Created by OSX on 10/25/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation


enum AppSettings : Int {
    case realTime = 2
    case SingleUpdate = 1
}

class LocalCache {
    
    class func getAppSettings() -> Int
    {
        return UserDefaults.standard.integer(forKey: "AppSettings")
    }
    
    class func saveAppSettings(flag:Int)
    {
        UserDefaults.standard.setValue(flag, forKey: "AppSettings")
    }
    
    
}

let hasBeenLaunchedBeforeFlag = "HasBeenLaunchedBeforeFlag"
extension UserDefaults {
    static func isFirstLaunch() -> Bool {
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
    
    static func isFirstRequest() -> Bool {
        let isFirstRequest = !UserDefaults.standard.bool(forKey: "isFirstRequest")
        if (isFirstRequest) {
            UserDefaults.standard.set(true, forKey: "isFirstRequest")
            UserDefaults.standard.synchronize()
        }
        return isFirstRequest
    }
}
