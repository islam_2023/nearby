//
//  LocationManager.swift
//  nearBy
//
//  Created by OSX on 10/25/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationManager: NSObject,  CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var viewController : HomeViewController?
    var locations = [CLLocation]()
    init(_ viewController : HomeViewController) {
        self.viewController = viewController 
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func setUp()  {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.allowsBackgroundLocationUpdates = true
        }

    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.locations.count == 0 {
        self.locations.append(locations[0])
            self.viewController!.setup()
        }
        else {
            if self.locations.count == 2 {
                self.locations[1] = locations[0]
            }
            else {
                self.locations.append(locations[0])
            }
        }
       DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
         let distance = self.totalDistance(of: self.locations)
        if distance >= 500 {
            self.viewController!.setup()
        }
       }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        Utility.alertNoInternetMessage(viewController!, type: "")
        
    }

    func totalDistance(of locations: [CLLocation]) -> CLLocationDistance {
        var distance: CLLocationDistance = 0.0
        var previousLocation: CLLocation?
        
        locations.forEach { location in
            if let previousLocation = previousLocation {
                distance += location.distance(from: previousLocation)
            }
            previousLocation = location
        }
        
        return distance
    }
    
}
