//
//  RealmHandeler.swift
//  nearBy
//
//  Created by OSX on 10/25/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation

import RealmSwift

class RealmHelper{
    
    let uiRealm = try! Realm()
    
    private init() {}
    
    public static let shared = RealmHelper()
    
    public func loadAllVenues() -> [Venue]{
        return Array(uiRealm.objects(Venue.self))
    }
    
    public func addNewVenue(venues: [Venue]) {
        do {
            for venue in  venues {
                
                try uiRealm.write {
                    uiRealm.add(venue)
                    
                }
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }

    }
    
    public func clearVenueItem(obj: Venue) {
        do {
            try uiRealm.write {
                uiRealm.delete(obj)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
    public func clearAllVenues() {
          do {
              try uiRealm.write {
                  uiRealm.delete(uiRealm.objects(Venue.self))
              }
          } catch let error as NSError {
              print("error - \(error.localizedDescription)")
          }
          
      }
    
    public func updateAllVenues(venues: [Venue]) {
        
        clearAllVenues()
        addNewVenue(venues: venues)
        
    }
    
    
}



extension RealmOptional : Encodable where Value: Encodable  {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        if let v = self.value {
            try v.encode(to: encoder)
        } else {
            try container.encodeNil()
        }
    }
}
extension RealmOptional : Decodable where Value: Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            self.value = try Value(from: decoder)
        }
    }
}
extension List : Decodable where Element : Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        var container = try decoder.unkeyedContainer()
        while !container.isAtEnd {
            let element = try container.decode(Element.self)
            self.append(element)
        }
    }
}
extension List : Encodable where Element : Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        for element in self {
            try element.encode(to: container.superEncoder())
        }
    }
}
