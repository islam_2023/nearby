//
//  Utility.swift
//  Oilco
//
//  Created by Eman Gaber on 24/05/18.
//  Copyright © 2018 Eman Gaber. All rights reserved.
//

import UIKit
import Foundation


class Utility: NSObject {
    
    static func UIColorFromRGB(rgbValue: UInt) -> UIColor
    {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    

    static func alertNoInternetMessage(_ viewDelegate : UIViewController , type : String)
    {
        DispatchQueue.main.async {
                  let rv = UIApplication.shared.keyWindow! as UIWindow
              let view = NoInternetView()
              view.delegate = viewDelegate as? NoInternetViewDelegate
              if type == "noData" {
                  view.alertImageView.image = UIImage(named: "searchNotFound")
                  view.messageLabel.text = "No Data Found !!"
              }
              rv.addSubview(view)
              view.frame = rv.bounds
        }
  
        
    }
    
    class var isConnectedToInternet:Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
}
