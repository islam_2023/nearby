//
//  NoInternetView.swift
//  Radary
//
//  Created by Eman Gaber on 4/11/18.
//  Copyright © 2018 Eman Gaber. All rights reserved.
//

import UIKit

protocol NoInternetViewDelegate {
    func closeNoInternetView(_ sender: NoInternetView)
}

class NoInternetView: UIView {
    
    var delegate: NoInternetViewDelegate?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var btnRetry: UIButton!


    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NoInternetView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
    }
    
    @IBAction func letsDoItAction(_ sender: UIButton) {
        self.removeFromSuperview()
        delegate?.closeNoInternetView(self)
    }
    
    

}
