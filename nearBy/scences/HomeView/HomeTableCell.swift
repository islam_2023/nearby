//
//  HomeTableCell.swift
//  nearBy
//
//  Created by OSX on 10/25/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit
import Kingfisher

class HomeTableCell: UITableViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    
    @IBOutlet weak var placeName: UILabel!
    
    @IBOutlet weak var placeDescribtion: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configre(veune : Venue)  {        
        if 0 != veune.categories.count , let url = URL(string: veune.categories[0].icon?.categoryIconUrl ?? "") {
            
            placeImage.kf.setImage(with: url, placeholder: UIImage(named: "icStarWhite"))
            placeDescribtion.text = veune.location?.address
            }
        placeName.text = veune.name
    }

}
