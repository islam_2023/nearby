//
//  ViewController.swift
//  nearBy
//
//  Created by OSX on 10/23/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var switchButton: UIBarButtonItem!
    lazy var locationManager = LocationManager(self)
    
    lazy var homeViewModel = HomeViewModel(showErrorViewDelegate: self, completion: {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    })
    
    
  override func viewDidLoad() {
        super.viewDidLoad()
    setUPSwichButton ()
    locationManager.setUp()
    }

    
    func setup()  {
        
        if Utility.isConnectedToInternet {
            let latLong = String(locationManager.locations[0].coordinate.latitude) + "," + String(locationManager.locations[0].coordinate.longitude)
            let param = ["ll":latLong]
            homeViewModel.LoadVeunes(parameters: param as NSDictionary)
        }
        else {
            let venues = RealmHelper.shared.loadAllVenues()
            homeViewModel.venues = venues
            self.tableView.reloadData()
            if venues.count == 0 {
                nodataView()
            }

        }
    }
    
    @IBAction func switchButton(_ sender: Any) {
        if LocalCache.getAppSettings() == AppSettings.SingleUpdate.rawValue {
               LocalCache.saveAppSettings(flag: AppSettings.realTime.rawValue)
               self.switchButton.title = "RealTime"
               locationManager.locationManager.allowsBackgroundLocationUpdates = true
               locationManager.locationManager.startUpdatingLocation()

           }
           else {
               LocalCache.saveAppSettings(flag: AppSettings.SingleUpdate.rawValue)
               self.switchButton.title = "SingleUpdate"
               locationManager.locationManager.allowsBackgroundLocationUpdates = false
               locationManager.locationManager.stopUpdatingLocation()
           }    }
    
    func setUPSwichButton(){
        if LocalCache.getAppSettings() == AppSettings.SingleUpdate.rawValue {
    
            self.switchButton.title = "SingleUpdate"
            locationManager.locationManager.allowsBackgroundLocationUpdates = false
            locationManager.locationManager.stopUpdatingLocation()
        }
        else {
            self.switchButton.title = "RealTime"
              locationManager.locationManager.allowsBackgroundLocationUpdates = true
                locationManager.locationManager.startUpdatingLocation()
        }
    }

}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return homeViewModel.venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as! HomeTableCell
        cell.configre(veune: homeViewModel.venues[indexPath.row])
        return cell
    }
    
    
}


extension HomeViewController : ShowErrorView {
    func nodataView() {
        Utility.alertNoInternetMessage(self, type: "noData")
    }
    
    func errorView() {
        Utility.alertNoInternetMessage(self, type: "")
    }
    
    
    
    
}
