//
//  HomeViewModel.swift
//  nearBy
//
//  Created by OSX on 10/25/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import SVProgressHUD

protocol ShowErrorView {
    func nodataView()
    func errorView()
}

class HomeViewModel {
    
    var venues = [Venue]()
    var showErrorViewDelegate : ShowErrorView?
    var completion : () -> ()?
    init(showErrorViewDelegate : ShowErrorView, completion : @escaping () -> ()) {
        self.completion = completion
        self.showErrorViewDelegate = showErrorViewDelegate
        
    }
    

    func LoadVeunes(parameters : NSDictionary? = ["ll" : "40.7,-74"])  {
        if UserDefaults.isFirstRequest() {

            SVProgressHUD.show()
            
        }
        client.search.venues(parameters: parameters as! [String : String]).response { result in
            switch result {
            case .success(let data):
                self.venues = Array(data.response.venues)
                DispatchQueue.main.async {
       RealmHelper.shared.updateAllVenues(venues: Array(data.response.venues))
                }
                self.completion()
                SVProgressHUD.dismiss()
                if self.venues.count == 0 {
                    self.showErrorViewDelegate?.nodataView()
                }
            case .failure(let error):
                self.showErrorViewDelegate?.errorView()
                print(error)
                
            }
        }
    }
    
    
    
    
}
